#include <iostream>
#include "myb.h"

#pragma GCC visibility push(hidden)
#include "mya.h"
#pragma GCC visibility pop

void hello_world_from_b() {
    std::cout << "Hello world from B calling hello_world_from_a" << std::endl;
    hello_world_from_a();
}
