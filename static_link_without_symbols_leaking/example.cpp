#include "myb.h"
#include "mya.h"
#include <iostream>

// Same symbol as in mya but we should not see its output
// If everthing is linked properly we will not see this call from mya and only from example
/*
void hello_world_from_a() {
    std::cout << "hello_world_from_a defined in example which we do not want to resolve to" << std::endl;
}
*/

int main() {
    std::cout << "Test" << std::endl;
    hello_world_from_b();

    // This will not link if the symbols from mya a hidden properly
    //hello_world_from_a();
    return 0;
}
