[![pipeline status](https://gitlab.com/fabio.sturm/conan-test/badges/master/pipeline.svg)](https://gitlab.com/fabio.sturm/conan-test/commits/master)

Conan
=====

### Links:

* https://conan.io

* https://docs.conan.io/en/latest/


CMake
=====

### Links:

* https://cmake.org

* https://cmake.org/cmake/help/latest/

* https://devdocs.io/cmake~3.6/

* https://gist.github.com/mbinna/c61dbb39bca0e4fb7d1f73b0d66a4fd1

* https://crascit.com/2015/07/25/cmake-gtest/

### Free Books:

[An Introduction to Modern CMake](https://cliutils.gitlab.io/modern-cmake/)


### Books:

[Professional CMake by Craig Scott](https://crascit.com/professional-cmake/)

[CMake Cookbook by Radovan Bast, Roberto Di Remigio](https://github.com/dev-cafe/cmake-cookbook/)


Demo Projects
=============

### dependant_libraries:

  This demonstrates how transitive dependencies can be made availbe during execution with a conan imports step

### identical_header_files

  With the default conan project you can't import the header file of a sepcific library, the modern way is to use cmake targets.


