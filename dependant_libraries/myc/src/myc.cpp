#include <iostream>
#include "myc.h"
#include "myb.h"

void myc(){
    myb();
    #ifdef NDEBUG
    std::cout << "Hello World Release from MyC!" <<std::endl;
    #else
    std::cout << "Hello World Debug from MyC!" <<std::endl;
    #endif
}
