#include <iostream>
#include "myb.h"
#include "mya.h"

void myb(){
    mya();
    #ifdef NDEBUG
    std::cout << "Hello World Release from MyB!" <<std::endl;
    #else
    std::cout << "Hello World Debug from MyB!" <<std::endl;
    #endif
}
